package pl.bash.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.bash.dao.QuotationDao;
import pl.bash.entity.Quotation;

@ManagedBean
@Component
public class QuotationController {

	@Autowired
	private QuotationDao quotationDao;
	private List<Quotation> quotations = new ArrayList<>();
	private List<Quotation> quotationsNegative = new ArrayList<>();
	private String content;

	@PostConstruct
	private void initQuotations() {
		quotations = quotationDao.findPositive();
		quotationsNegative = quotationDao.findNegative();
	}

	public String saveQuotation() {
		Quotation quotation = new Quotation();
		quotation.setContent(content);
		quotation.setDate(Calendar.getInstance().getTime());
		quotationDao.save(quotation);
		return "/pages/home";
	}

	public void addRate(int id) {
		Quotation currentQuotation = quotationDao.getById(id);
		currentQuotation.setRate(currentQuotation.getRate() + 1);
		quotationDao.save(currentQuotation);
	}

	public void minusRate(int id) {
		Quotation currentQuotation = quotationDao.getById(id);
		currentQuotation.setRate(currentQuotation.getRate() - 1);
		quotationDao.save(currentQuotation);
	}

	public List<Quotation> getQuotations() {
		quotations = quotationDao.findPositive();
		return quotations;
	}

	public void setQuotations(List<Quotation> quotations) {
		this.quotations = quotations;
	}

	public List<Quotation> getQuotationsNegative() {
		quotationsNegative = quotationDao.findNegative();
		return quotationsNegative;
	}

	public void setQuotationsNegative(List<Quotation> quotationsNegative) {
		this.quotationsNegative = quotationsNegative;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}